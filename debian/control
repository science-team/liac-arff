Source: liac-arff
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Christian Kastner <ckk@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/renatopp/liac-arff
Vcs-Git: https://salsa.debian.org/science-team/liac-arff.git
Vcs-Browser: https://salsa.debian.org/science-team/liac-arff

Package: python3-liac-arff
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: library for reading and writing ARFF files in Python
 The liac-arff module implements functions to read and write ARFF files in
 Python. It was created in the Connectionist Artificial Intelligence
 Laboratory (LIAC), which takes place at the Federal University of Rio Grande
 do Sul (UFRGS), in Brazil.
 .
 ARFF (Attribute-Relation File Format) is an file format specially created for
 describing datasets which are used commonly for machine learning experiments
 and software. This file format was created to be used in WEKA, the best
 representative software for machine learning automated experiments.
